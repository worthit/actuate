/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worthit.actuate.congress;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;
/**
 *
 * @author delicia
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Legislator {

    String bioGuideId;
    Date birthDay;
    Chamber chamber;
    String contactForm;
    String crpId;
    String district;
    String facebookId;
    String fax;
    String[] fecIds;
    String firstName;
    String lastName;
    String middleName;
    String suffix;
    String leadershipRole;
    Gender gender;
    String govTrackId;
    boolean inOffice;
    String lisId;
    String nickName;
    String openCongressEmail;
    String ocdId;
    String office;
    Party party;
    String phone;
    int senateClass;
    String state;
    String stateName;
    String stateRank;
    Date termStart;
    Date termEnd;
    String thomasId;
    String title;
    String twitterId;
    String website;

}
