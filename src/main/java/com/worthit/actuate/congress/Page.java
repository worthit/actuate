/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.worthit.actuate.congress;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 *
 * @author delicia
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Page {
    private int count;
    private int perPage;
    private int page;
}
