/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worthit.actuate.congress;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author delicia
 */
public class CongressAPI {

    private RestTemplate restTemplate = new RestTemplate();
    private String apikey = "4fca4c26f391493bb0fe99d867379e94";
    private String urlLegistlators = "https://congress.api.sunlightfoundation.com/legislators";
    private String urlLegistlatorsForLocation = "https://congress.api.sunlightfoundation.com/legislators/locate";
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<String> entity;

    public CongressAPI() {
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("X-APIKEY", apikey);
        entity = new HttpEntity<String>("parameters", headers);

    }

    public List<Legislator> findLegislators(String postalCode) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("zip", postalCode);
        params.put("apikey", apikey);
        System.out.println("***************************************************");
        System.out.println("findLegislators postalCode=" + postalCode);
        System.out.println("findLegislators URL=" + urlLegistlatorsForLocation + "?zip=" + postalCode + "&apikey=" + apikey);
        restTemplate.exchange(urlLegistlatorsForLocation + "?zip=" + postalCode + "&apikey=" + apikey, HttpMethod.GET, entity, String.class);
        CongressResponse response = restTemplate.getForObject(urlLegistlatorsForLocation, CongressResponse.class, params);
        System.out.println("findLegislators response=" + response);
        System.out.println("***************************************************");
        return response.getResults();
    }

    public List<Legislator> findLegislators(double latitude, double longitude) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("latitude", Double.toString(latitude));
        params.put("latitude", Double.toString(latitude));
        params.put("apikey", apikey);
        CongressResponse response = restTemplate.getForObject(urlLegistlatorsForLocation, CongressResponse.class, params);

        return response.getResults();
    }
}
