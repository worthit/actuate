/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.worthit.actuate.congress;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.Data;

/**
 *
 * @author delicia
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CongressResponse {
    private int count;
    private Page page;
    private List<Legislator> results;
}
