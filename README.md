
# ![actuate](https://placeholdit.imgix.net/~text?txtsize=73&bg=ffffff&txtclr=3b5998&txt=ac%E2%80%A2tu%E2%80%A2ate&w=350&h=150)
**/ˈak(t)SHəˌwāt/** *to incite or move to action*

[![MIT License](https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square)](/LICENSE) 

- [Quick start](#Quick)
- [Installation](#Installation)
- [Features](#Features)
- [Usage](#Usage)
- [Support](#Support)
- [Documentation](#Documentation)
- [Contributing](#Contributing)
- [Inspiration and Alternatives](#Inspiration)
- [Credits](#Credits)
- [License](#License)

Actuate is a _________ for __________.

* Homepage: https://dkbrummitt.bitbucket.org/actuate
* Source: https://bitbucket.org/worthit/actuate/src
* Twitter: [@actuate](https://twitter.com/dkb-actuate)

## Quick start 

Describe how the user can quickly get started using Actuate

## Installation
Describe the full installation process if different from Quick Start. Otherwise omit this section.

```sh
curl -LO http://example.com/foo/bar/
git add README.md
git commit -m "Use README Boilerplate"
```
## Features 
* Feature 1
* Feature 2
  * Feature a
  * Feature b
* Feature 3

## Usage
CD to the working directory of your existing project. 
```
cd /path/to/project
```
Clone the Readme Template
```
git clone https://worthit@bitbucket.org/worthit/actuate.git
```
Replace the Actuate of this `README.md` with your project's name. 
Replace all actuate with your project's repo or twitter handle. 
Adjust the description, logo placeholder, Quick Start, etc. to fit your project.

Feel free to add, update, or remove any sections to fit your project.   

## Support

Please [open an issue](https://bitbucket.org/dkbrummitt/actuate/issues/new) for support.

## Documentation
Take a look at the documentation table of contents. This documentation is bundled with the project, which makes it readily available for offline reading and provides a useful starting point for any documentation you want to write about your project.

## Contributing 

Please contribute using [Github Flow](https://guides.bitbucket.org/introduction/flow/). Create a branch, add commits, and open a pull request.
Anyone and everyone is welcome to contribute, however, if you decide to get involved, please take a moment to review the [guidelines](/CONTRIBUTING.md):
* Bug reports
* Feature requests
* Pull requests

## Inspiration and Alternatives
Actuate was inspired by…

Read Me Template was inspired by a [blog post](https://changelog.com/top-ten-reasons-why-i-wont-use-your-open-source-project/) on [The Changelog](https://changelog.com/).
The [contributing](/CONTRIBUTING.md) file was heavlily influenced by [HTML 5 boilerplate](https://bitbucket.org/h5bp/html5-boilerplate/blob/master/CONTRIBUTING.md). The README.md is an amalgamation of several different READMEs. The "features" included were picked because of common practices, design, and UX.
Future versions may include some of the recommendations found on [StackOverflow](http://stackoverflow.com/questions/2304863/how-to-write-a-good-readme)

A few Alternatives are:
* Alternative 1
* Alterative 2

The best resource for finding more alternatives is Google

## Credits
* Markdown derived from [ReadMe Template](https://github.com/dkbrummitt/readme-template)

## License 
The code is available under the [MIT license](/LICENSE.md).

Copyright (c) 2016 Delicia Brummitt

